# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=pueue
pkgver=1.0.0
pkgrel=0
pkgdesc="Manage your shell commands"
url="https://github.com/nukesor/pueue"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
arch="$arch !ppc64le" # Fails to build 'ring v0.16.19'
license="MIT"
checkdepends="bash"
makedepends="cargo"
source="https://github.com/Nukesor/pueue/archive/v$pkgver/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF
}

build() {
	cargo build --release --locked
}

check() {
	cargo test --locked
}

package() {
	cargo install --locked --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
90b8e96a52e869ac55a976eec9f81cdd1946c350899fc69bc5e8158cca810ad340e5264f61892628ae181ad58206a8ae50e6a73d3b820d875f01aa06df3fecee  pueue-1.0.0.tar.gz
"
