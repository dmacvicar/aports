# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=2.3.18
pkgrel=0
pkgdesc="Blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
makedepends="cmake doctest-dev ffmpeg-dev libunistring-dev linux-headers ncurses-dev ncurses-terminfo readline-dev zlib-dev"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="https://github.com/dankamongmen/notcurses/archive/v$pkgver/notcurses-$pkgver.tar.gz
	https://github.com/dankamongmen/notcurses/releases/download/v$pkgver/notcurses-doc-$pkgver.tar.gz
	"

# Don't run tests on mips64, builder is too slow.
[ "$CARCH" = "mips64" ] && options="!check"
[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	env TERM=vt100 CTEST_OUTPUT_ON_FAILURE=1 make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	for i in 1 3 ; do
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec echo "$pkgdir"/usr/share/man/man$i {} \;
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec install -Dm644 -t "$pkgdir"/usr/share/man/man$i {} \;
	done
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/bin/notcurses-info
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="
9b3d695e71014799330a5be77ee1e9e43d85e1bcbf9657c516f4c6ca6076651e3b40408fb3212cccfdfefc5643b6f504f7536eb674982049485effbb9e2233d4  notcurses-2.3.18.tar.gz
27dba504bf873680a7f2ca36739002d39d4e5b0f913d64b10926dda66a54d58eb019c7199f93ab04a6b7fb83ba3d646369e3a96d95a5ad548b7bd25f784bf823  notcurses-doc-2.3.18.tar.gz
"
